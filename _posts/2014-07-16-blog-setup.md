---
layout: post
title: Blog setup
comments: true
---

This blog is created by modifying [http://joshualande.com/](https://github.com/joshualande/joshualande.github.io). Thanks [poole](https://github.com/poole/poole), @joshualande and your [post](http://joshualande.com/jekyll-github-pages-poole/).

## My main modifications

### Display multiple short posts in one page

in index.html, replace the line
 
{% raw %}
	{{post.content}}
{% endraw %}
	
with

{% raw %}
	{{ post.excerpt }}
	<a href="{{ post.url }}">
	[ read more ... ]
	</a>
{% endraw %}

and in _config.yml, edit the line

	paginate:   posts-per-page
	
posts-per-page is the number posts you want to display in one page, e.g. 10.

### Show twitter share for every post

in layout/post.html, add

{% raw %}
	{% include twitter_plug.html %}
{% endraw %}

after {% raw %}{{ content }}{% endraw %}